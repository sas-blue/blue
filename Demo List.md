Prep - folders are in their own git repos (local-only is fine) so they can be rolled back after each change.

subvis folder - normal subvis with some in a subfolder and with some craziness - colors, large icons, etc. in git so I can rollback after each.
GUIDemo - one with GUI tag, one with splitter, one subpanel, one tab

IN Subvis folder
- run with nothing to get help
- show version command
- no options, run on dot folder
- use -v
- use -vv
- -d for debug (include for all further demos)
- --list
- -d --ignore single
- -d --ignore list
- -d --only single
- -d --only list
- Add a single #blueignore tag to a vi 
- Add #blueignore with a list
- Add #blueignore GUI macro
- Add #blueignore ALL
- add .blueignore file to subdirectory - run on entire folder and show result.
- add .blueignore to main directory. Show additiveness for subdirectory

NOW switch to GUI folder
- Run on GUI Demo folder - show #GUI tag expansion - point out auto detect.
