# Blue - Like Black, but for LabVIEW

[![Image](https://gitlab.com/sas-blue/blue/-/badges/release.svg)](https://gitlab.com/sas-blue/blue/-/releases) [![Image](https://gitlab.com/sas-blue/blue/badges/main/pipeline.svg)](https://gitlab.com/sas-blue/blue/-/pipelines) [![Image](https://www.vipm.io/package/sas_workshops_lib_blue_formatter_for_labview/badge.svg?metric=installs)](https://www.vipm.io/package/sas_workshops_lib_blue_formatter_for_labview/) [![Image](https://www.vipm.io/package/sas_workshops_lib_blue_formatter_for_labview/badge.svg?metric=stars)](https://www.vipm.io/package/sas_workshops_lib_blue_formatter_for_labview/)


This a LabVIEW autoformatter inspired by Python's [Black](https://github.com/psf/black) and [this](https://gitlab.com/felipe_public/lv-format-on-save) from Felipe Pinheiro Silva. I also borrowed some code from the various [Nattify VIs](https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Quick-Drop-Keyboard-Shortcut-Nattify-VI/m-p/3990402)

![BlueTool](imgs/ToolsMenu.webp)

## Table of Contents

[TOC]

## Roadmap

Right now this just implements all the Nattify functions. My goal is to add many more formatting steps in the future. All of the Nattify functions are currently hard-coded in. However there is a nice class heirarchy and my long-term plan is to consider making it a plugin based system where it ships with certain plugins but you can edit them and/or supply your own.

## Requirements

- G-CLI
- LabVIEW 2024Q3 or newer

## Installation

The easiest way to install Blue is via VIPM. Just search for the Blue Formatter and it should come up - **NOTE** there are a lot of Blue Origin packages there to sort through. You can also get it directly from [VIPM.io](https://www.vipm.io/package/sas_workshops_lib_blue_formatter_for_labview/)

You can also [build from scratch](#build-from-scratch).

## Quick Start

### Tools Menu

There is a Blue AutoFormatter entry in the tools menu. It launches a simple GUI.

![BlueTool](imgs/ToolsMenu.webp)

#### File Watching

By Default, File Watching is turned ON. In this mode Blue pays attention to the active project. It watches the directory of that project (and all subdirectories). It also pays attention to the Active VI. These are displayed along the bottom of the GUI. When Blue notices that you have saved the Active VI, then it formats that VI automatically. It only works on the active VI, saving the project or classes or Libraries does not trigger anything and it does not any .lvlib, .lvclass or .lvproj files.

#### Browse

This let's you select any individual file or folder and run Blue on it. If file watching mode is on, it will be paused until this done and then restarted.

#### All Files in Project

You can run Blue on all the files in your project simply by clicking the button. If file watching mode is on, it will be paused until this done and then restarted.

#### Changed Files

If the Tools Menu GUI detects that your code is in a git repository, it also gives the option to run Blue only on changed files. Selecting this option will allow you to select or enter a Git branch or ref. The tool will then run Blue on just the files that have changed since you diverged from that branch. This is useful to avoid stepping on others toes when working in parallel with others. It also helps to keep commits small and targeted. If file watching mode is on, it will be paused until this done and then restarted.

#### Status
The status gets updated every time a file is processed.


### From the terminal using G CLI

Running from the terminal is a quick and easy way to format an entire source code directory.

```
g-cli blue -- "Source Code Directory"
```

#### Running only on changed files

The Git Bash command below will run blue only on files that have changed or been added to your working directory(using the default ignore file). The reference point is HEAD - you can change that to point to a local or remote branch if you'd like. You can also any g-cli options (for example to specify a specific version or bitness of LabVIEW) between the `g-cli` and `blue` at the end of the line. You can specify any options at the end after the double dash (for example to set the verbosity).
 
```
(git diff --name-only --diff-filter=MA HEAD; git ls-files --others --exclude-standard) | xargs -d '\n' g-cli blue --
```

## CLI Usage

Blue was originally designed to be used as a CLI Tool. The simplest way to call it from the CLI is:
```
g-cli blue -- "Source Code Directory"
```
It will find every VI in the Source Code directory and run all the formatting steps on each one with the exceptions of GUIs. Blue will skip some tests if [it thinks the VI is a GUI](#gui-autodetect). 

### Help

If you just type `g-cli blue.vi` in your favorite shell with no options, you should see something like this, which will show you all the CLI options:

```
Blue - Like Black but for LabVIEW
Version: 1.0.0
Path: C:\Users\SAS\Desktop\lv-format-on-save\Source\blue.vi

Blue is an autoformatter for LabVIEW.

ADMINISTRATIVE OPTIONS
NOTE: These don't process files, just do their thing and exit
--help, -h      Help Information
--version       Version Information
--list, -l      List All Available Steps and Macros
--path, -p      Path where blue.vi was found - mostly for debugging
--description   Get the description for a Step or Macro


FILE PROCESSING OPTIONS
--only, -o      Only run the specified tests. All ignores still apply. Will accept a CSV list. Place the entire list in double quotes for best results.
--ignore, -i    Ignore the specified tests. Will accept a CSV list. Place the entire list in double quotes for best results.
--force, -f     Often used with --only Runs tests regardless of ignores. Use with caution.

VERBOSITY
-q      Quiet
-v      Verbose
-vv     Very Verbose
-vvv or -d      Debug

USAGE
g-cli blue -- [OPTIONS] ["path to directory or file to format"]
```

### Formatting a directory

Blue is intended to be used to format a directory of VIs. So typically you'd use it like this:

``` 
g-cli blue -- "Directory to Format" 
```

"Directory to Format" can be an absolute or relative path. 

**NOTES** 
- Take note of the quotes around the path. You will definitely need them if your path has spaces in it.
- Instead of a directory you can point to single file. 
- You can also pass multiple paths (seperated by spaces). Each of these can point to a file or a directory.
- At the moment blue only operates on VIs with a diagram. It doesn't yet do anything for controls, classes, libraries, project files, etc. It may soon.

Without supplying any other options, the output will look something like this:

```
4 Files Processed 2 Formatted
```

The count of files is just the number of vi files in the directory. Formatted means that LabVIEW detected a change after the formatting code ran and decided to save the file.

### Verbosity

There are several verbosity options.

#### Quiet

`-q` or `--quiet` suppresses the CLI output.

#### Verbose

`-v` is for verbose mode. It simply lists the files that are being processed.

The output might look similar to this:
```
Processing File -> C:\Users\SAS\Desktop\Demo\GUI.vi
Processing File -> C:\Users\SAS\Desktop\Demo\Testing.vi
VI Saved -> C:\Users\SAS\Desktop\Demo\Testing.vi
Processing File -> C:\Users\SAS\Desktop\Demo\Try2.vi
VI Saved -> C:\Users\SAS\Desktop\Demo\Try2.vi
Processing File -> C:\Users\SAS\Desktop\Demo\trying.vi
VI Saved -> C:\Users\SAS\Desktop\Demo\trying.vi
4 Files Processed 3 Formatted
```

`VI Saved` is used to indicate the specific VIs that were saved after being formatted. 

#### Very Verbose

`-vv` is for very verbose mode. It lists the files and the formatting steps run on each.

The output will look something like this:
```
Processing File -> C:\Users\SAS\Desktop\Demo\Testing.vi
Step Starting -> Seperate Compiled Code From Source File
Step Starting -> Auto Error Handling
Step Starting -> Autogrow
Step Starting -> Front Panel Color
Step Starting -> Icon View
Step Starting -> Move Error Wires Back
Step Starting -> Remove No Error
Step Starting -> Subdiagram Label Justify
Step Starting -> Terminal Labels on Side
Step Starting -> Terminal Labels Transparent
Step Starting -> Arrange FP
Step Starting -> Arrange BD
VI Saved -> C:\Users\SAS\Desktop\Demo\Testing.vi
1 Files Processed 1 Formatted
```

If any tests or being [ignored](#ignoring-specific-steps), they simply won't show up in this list.

#### Very Very Verbose or Debug

`-vvv` or `-d` is for debug mode. This will provide much more information, including timing information and what tests are being ignored.

The output will look something like this:

```
Processing File -> C:\Users\SAS\Desktop\Demo\Testing.vi
GUI Detected -> C:\Users\SAS\Desktop\Demo\Testing.vi
Ignored Tests -> Front Panel Color,Arrange FP,Arrange BD
Step Starting -> Seperate Compiled Code From Source File
Step Duration -> 0 ms
Step Starting -> Auto Error Handling
Step Duration -> 1 ms
Step Starting -> Autogrow
Step Duration -> 11 ms
Step Starting -> Icon View
Step Duration -> 12 ms
Step Starting -> Move Error Wires Back
Step Duration -> 13 ms
Step Starting -> Remove No Error
Step Duration -> 0 ms
Step Starting -> Subdiagram Label Justify
Step Duration -> 9 ms
Step Starting -> Terminal Labels on Side
Step Duration -> 19 ms
Step Starting -> Terminal Labels Transparent
Step Duration -> 18 ms
Processing Time -> C:\Users\SAS\Desktop\Demo\Testing.vi 142 ms
1 Files Processed 0 Formatted
Total Time -> 2365 ms
```

Note this adds timing information. It lists the steps being ignored. It also alerts you to the fact that this particular VI was [automatically detected as a GUI](#gui-autodetect). It will also tell you what [ignore bookmarks](#using-bookmarks) were found as well as any [ignore files](#using-blueignore-files) found.

## Ignoring Specific Steps

There may be times when you don't want to run all the formatting steps. There are several ways to do this to suit several different use cases. Running with the verbosity mode set to debug will tell you exactly which steps were ignored.

### Getting a List of Steps and Macros

The first step in ignoring some steps is to figure out all the names of the steps. `g-cli blue -- --list` will produce something like this:

```--STEPS--
Seperate Compiled Code From Source File
Auto Error Handling
Autogrow
Front Panel Color
Icon View
Move Error Wires Back
Remove No Error
Subdiagram Label Justify
Terminal Labels on Side
Terminal Labels Transparent
Arrange FP
Arrange BD

--MACROS--
GUI
ALL
```

The [macros](#macros) represent a series of steps. You can use macros interchangeably with step names. To see what they expand to, see [below](#description)

#### Description

There is a description option. You pass it the name of a step or macro and it will give you a description and for steps, will tell you where to locate the code for that step so you can inspect the scripting code if you would like.

As an example `g-cli blue -- --description Autogrow` produces:

```
Turn off auto grow on structures.
Located at C:\Users\SAS\Desktop\lv-format-on-save\Source\Steps\Autogrow\Autogrow.lvclass
```

Of course on your computer, the location may be different.

### Using the CLI --ignore Flag

You can easily ignore a single step, a [macro](#macros), or a list of steps using the `--ignore` option on the CLI.

Some examples:
```
g-cli blue -- --ignore "GUI, Arrange BD" "source code directory"
# used to ignore any steps specified by the GUI Macro and the Arrange BD step
g-cli blue -- --ignore "Autogrow" "source code directory"
# used to ignore the Autogrow step"
```

**NOTE** Since the step names have spaces we need quotes around the step list whether it is a single step or a comma seperate list of steps.

### Using the CLI --only flag

You can easily just run a single step, a [macro](#macros), or a list of steps using the `--only` option on the CLI. This behaves in the opposite manner to the `--ignore` option. 

Some examples:
```
g-cli blue -- --only "GUI, Arrange BD" "source code directory"
# used to only run any steps specified by the GUI Macro and the Arrange BD step
g-cli blue -- --only "Autogrow" "source code directory"
# used to only run the Autogrow step"
```

**NOTE** Since the step names have spaces we need quotes around the step list whether it is a single step or a comma seperate list of steps. Also if one of the other igore is used, that will take precedence. For example if you use `--only` to run the Autogrow step, any VIs with a `#blueignore Autogrow` bookmark will still ignore the Autogrow step and it won't be run on that VI even though it was specified using the `--only` option.

### Using the CLI --force flag

You can use this flag to run all tests or a set of tests (when combined with the [only](#using-the-cli-only-flag) option) regardless of any ignores from any source. **Use this with caution.**

### Using Bookmarks

You can ignore steps for a specific VI simply by adding a #blueignore bookmark. Just drop a string constant on the block diagram and then type in #blueignore followed by either a single step name or a CSV list. You can have multiple bookmarks in one VI. They all get combined. In debug mode, the CLI output will tell you what Bookmarks were found for each VI.

### Macros

There are currently 2 built-in Macros. In the future there may be more and custom macros may be allowed.

#### All

ALL refers to all steps. If this is found, then no steps are processed. A typical use case would be a [.blueignore file](#using-blueignore-files) to skip formatting an entire directory or as a [bookmark](#using-bookmarks) to skip formatting a particular VI.

#### GUI

GUI expands to "Arrange FP, Front Panel Color". It is useful to skip these steps if a VI is a GUI since they affect the appearance that the user sees. 

##### GUI Autodetect

Blue will automatically detect that a VI is a GUI and skip the GUI steps if the VI has any of the following:

- splitter bar
- subpanel
- tab control
- altered window setting such as being modal or transparent.

This should catch most cases, but if you are displaying a VI inside a subpanel it may not have any of these characteristics, in which case you should just use a [bookmark](#using-bookmarks). There is no harm if you have a `#blueignore GUI` bookmark and blue detects that the VI is a GUI, so you can always add a `#blueignore GUI` bookmark to be conservative. 

### Using .blueignore files

Blueignore files work like .gitignore files. You can create them in any directory and they apply to any files in that directory or any of it's subdirectories. You just include a list of steps to ignore, one per line. 

**NOTE** It is one step per line, CSV lists won't work here. 

Blueignore files are additive, so if you are in a subdirectory, the list of steps to ignore for that subdirectory gets added to the list from each parent directory - if there is one. Therefore if there is one particular step you really don't like and never want to run, you could create a .blueignore on your root directory that includes the name of that step.

## Build From Scratch


You can get the latest and greatest version by building from scratch. Before building from scratch, you should make sure the CI passes. I'm pretty good about not breaking it and occassionally I do. If the CI passes you can be pretty sure that it is good to go. In fact, when I release to VIPM I don't do any additional testing, so it's the same level of testing as the official VIPM release. It's pretty thorough.

To build from scratch, you have 2 options:

### build.sh

Open Git Bash in the project directory and enter `./build.sh 1` The '1' is the build number. You can replace that with whatever you want. This will install the vipc, run the unit tests, and build the package, which you can then find in the build directory and manually install.

### _local.sh

This script will not only run the build script referenced above but also install the package and run a set of install tests using pytest. Before running the script you'll have to install python (In my CI process I am using python 3.12 - it will probably work in other versions.) Once you have done that, then run `./_local.sh 1` in Git Bash from the project directory. Again the '1' is the build number and can be replaced with whatever you want.

## TroubleShooting

If you have any issues, you can try running with -d to turn on debug mode. The CLI will spit out a bunch of information that may help track down your problem. If you need further help, run blue using -d and copy the results from the CLI including the prompt that tells you what directory you are in and what command you ran and we'll take a look.

## Contributing

At this point if you are interested in contributing, just email me. 



