--VI STEPS--
Separate Compiled Code From Source File
Auto Error Handling
Autogrow
Front Panel Color
Icon View
Move Error Wires Back
Remove No Error
Subdiagram Label Justify
Terminal Labels on Side
Terminal Labels Transparent
Replace String Constants
String Constant Display Style
Numeric Constant Show Radix
Hide Unwired Iteration Terminals
Arrange FP
Arrange BD

--LIBRARY STEPS--
Separate Compiled Code For Library
Remove Class Mutation History
Remove Lvclass and Lvlib From Localized Name

--PROJECT STEPS--
Separate Compiled Code For Project

--MACROS--
ALL
GUI
NATTIFY
