﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="24008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Steps.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Steps.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">*!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Nattify</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">604012544</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Align 1 Row of FP Controls.vi" Type="VI" URL="../Align 1 Row of FP Controls.vi"/>
	<Item Name="Arrange BD.vi" Type="VI" URL="../Arrange BD.vi"/>
	<Item Name="Arrange FP Controls Columns.vi" Type="VI" URL="../Arrange FP Controls Columns.vi"/>
	<Item Name="Arrange FP Controls Rows.vi" Type="VI" URL="../Arrange FP Controls Rows.vi"/>
	<Item Name="Arrange FP Controls.vi" Type="VI" URL="../Arrange FP Controls.vi"/>
	<Item Name="Arrange FP.vi" Type="VI" URL="../Arrange FP.vi"/>
	<Item Name="Arrange Other FP Objects.vi" Type="VI" URL="../Arrange Other FP Objects.vi"/>
	<Item Name="Compute Bound on Grid.vi" Type="VI" URL="../Compute Bound on Grid.vi"/>
	<Item Name="Compute Top Row Vertical Delta.vi" Type="VI" URL="../Compute Top Row Vertical Delta.vi"/>
	<Item Name="Compute Window Bounds.vi" Type="VI" URL="../Compute Window Bounds.vi"/>
	<Item Name="Ensure Leftmost BD Object Is Visible.vi" Type="VI" URL="../Ensure Leftmost BD Object Is Visible.vi"/>
	<Item Name="Enum Choice Array.vi" Type="VI" URL="../Enum Choice Array.vi"/>
	<Item Name="Establish Window Bounds.vi" Type="VI" URL="../Establish Window Bounds.vi"/>
	<Item Name="Filter FP Conn Pane Controls.vi" Type="VI" URL="../Filter FP Conn Pane Controls.vi"/>
	<Item Name="FP Control Info.ctl" Type="VI" URL="../FP Control Info.ctl"/>
	<Item Name="FP Objects Arrangement Info.ctl" Type="VI" URL="../FP Objects Arrangement Info.ctl"/>
	<Item Name="Get BD Canvas Margin.vi" Type="VI" URL="../Get BD Canvas Margin.vi"/>
	<Item Name="Get Display Workspace Bounds.vi" Type="VI" URL="../Get Display Workspace Bounds.vi"/>
	<Item Name="Get FP Controls Max Bottom.vi" Type="VI" URL="../Get FP Controls Max Bottom.vi"/>
	<Item Name="Get FP Controls Rows.vi" Type="VI" URL="../Get FP Controls Rows.vi"/>
	<Item Name="Get FP Objects Arrangement Info.vi" Type="VI" URL="../Get FP Objects Arrangement Info.vi"/>
	<Item Name="Get Min Window Dimensions.vi" Type="VI" URL="../Get Min Window Dimensions.vi"/>
	<Item Name="Get Window Gaps.vi" Type="VI" URL="../Get Window Gaps.vi"/>
	<Item Name="Get Window INI tokens.vi" Type="VI" URL="../Get Window INI tokens.vi"/>
	<Item Name="Get Window Margins.vi" Type="VI" URL="../Get Window Margins.vi"/>
	<Item Name="Min Window Dimensions.ctl" Type="VI" URL="../Min Window Dimensions.ctl"/>
	<Item Name="Move All BD Objects.vi" Type="VI" URL="../Move All BD Objects.vi"/>
	<Item Name="Order FP Conn Pane Controls.vi" Type="VI" URL="../Order FP Conn Pane Controls.vi"/>
	<Item Name="Rectangle Dimensions.ctl" Type="VI" URL="../Rectangle Dimensions.ctl"/>
	<Item Name="Resize BD.vi" Type="VI" URL="../Resize BD.vi"/>
	<Item Name="Resize FP.vi" Type="VI" URL="../Resize FP.vi"/>
	<Item Name="Space FP Controls.vi" Type="VI" URL="../Space FP Controls.vi"/>
	<Item Name="Window Gaps.ctl" Type="VI" URL="../Window Gaps.ctl"/>
	<Item Name="Window Margins.ctl" Type="VI" URL="../Window Margins.ctl"/>
	<Item Name="Window Type.ctl" Type="VI" URL="../Window Type.ctl"/>
</Library>
