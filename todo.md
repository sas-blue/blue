- [?] Add ability to add meta bookmarks that are kind of batch things that allow you to disable multiple fixes.
- [ ] Allow for modifying class, library, and project files (seperate compiled code is easy and remove lvclass from class name).
- [ ] Add support for ctls - maybe arrange vertically for clusters... - perhaps only if no arrangement already selected.
- [ ] constant labels
- [ ] scroll events and cases to frame 0, if boolean - scroll to true - maybe? or allow user to set whichever one to 0
- [ ] enforce caseing (camel, undersscore?) on inputs and outputs. Hard to do for classes/libraries, etc.
- [ ] figure out control labels on BD - transparency, location.
- [ ] figure out control labels on FP - maybe turn of captions and jsut use labels (ignore for FPs that are visible).
- [ ] consider if FP/GUI do check and turn on captions for controls.
- [ ] Account for various LV Versions when launching G-CLI from Python monitor.
- [ ] Add to bluemon ability to specify CLI Options
- [ ] for events/case structure - sort frames before setting to 0
- [ ] consider adding code to library/class and project that if it references files outside its directory to move them into the directory... - add flag for this behavior.
- [ ] Add change window appearance properties to GUI autodetect logic 
- [ ] Consider making GUI expansion generic and recursive, so we can nest them - maybe_? Certainly want to be able to create custom ones
- [ ] Hide unwired iteration terminals in loops
- [ ] Hide unwired event terminals inside event frames
- [ ] for GUI check consider scroll bar changes... maybe?
- [ ] add ability to add user ignore sets/macros
- [ ] go back to Nattify forum page. look at variations like centering controls - not sure if this is good idea. I like no options... Any color you want as long as it is black.
- [ ] Add code to move controls to left and align on R edges and indicators to right and align on L edges (on main bd) 
- [ ] Consider adding an option to run only on changed files (use git to determine)
- [ ] Add GUI to Tools menu to either run once or generate CLI string to be copied to clipboard - so need to remember flags
- [ ] Add support for classes and removing mutation history (and probably also remove .lvclass from name in property dialog). This will force opening up the types of files handled and force rearrangnig the class heirarchy. Also consider seperating out Steps that need block diagram and those that don't. 
- [ ] Add Joerg's logging lib and add option to log instead of displaying to screen and stopping. - maybe add logfile to ini file - if defined will log. add stop on error flag.
- [ ] rename generic tests to core tests.
- [ ] controls to L side and R align
- [ ] controls to R side and L align
- [ ] Add logging to GUI and display errors - maybe?
- [ ] Issue with PPLs and paths to VIs inside PPLs. Obviously can't do anything with those. Generally not an issue using it from CLI or when detecting changes from Git, but when running from project and selecting all files, then can be an issue due to the way the project file is parsed for paths. May not be an issue due to the fact that they don't have BDs - although if debugging is enabled do they? Needs testing.
- [ ] Update screenshots of tools menu tool in readme, now that I added a title bar.:wq


NEW 11/26/24 - Current Release
- [x] Go back over previous list, decide what to keep and get rid of. No need to keep what has already been done.
- [x] **Add code after generating directory map of ignores and if ignoring ALL just filter anything in those directories out so it just doesn't process them at all. Will be much faster.**
- [x] Add ignore list to Library Steps and test
- [x] Figure out force for library steps. - Needs test
- [x] Library steps need added to list command as well
- [x] Potential Bug - Explore vit and x02 option. Currently only verifying that step runs, not results. Could be running on a new VI and not VIT itself. 
- [ ] inlining Step Factory - need to remove or cleanup afterwards.
- [ ] Consider getting rid of apply ignores.VI - only used in tests.
- [x] PyTest for only Separate Compiled Code from Libraries
- [x] Pytests for library stuff
- [x] Make sure ignore Separate Compiled Code for Library works.
- [ ] Add pytest for library and files - one with ignoring a Separate compiled code from libraries and one ignoring a VI step - both from CLI.

Upcoming Releases
- [x] Add project filetype and steps - initial step is separate compiled code
- [ ] Add remove class mutation history
- [ ] Add remove .lvlib and .lvclass from localization string
- [ ] Reconsider how we handle ignores - We should calculate anything we can before opening the file and if the list is empty for that type then we should not open the file at all.
- [ ] Add macros for Project, Library, and VI 
- [ ] Fix Typo for Seperate Compiled Code From Source File - add in somewhere a replace so old refs still work.
- [ ] Move away from Pytest. Slow. Requires build step. Doesn't Scale well. Maybe just keep the help to make sure everything gets installed in the correct place and G-CLI can find it.
- [ ] Idea - make step list, that includes multiple different steps and make it composable. Make VIs, libraries, etc. names associated with different lists. This part a little less clear.
- [ ] list interface for both ignore and only.
- [ ] Expanding Macros needs to go inside the list class
- [ ] Rename ignore list filter list so it could apply to ignore or only


Integrating blue watcher
- [ ] rework existing interface - remove selector
- [ ] don't exit immediately
- [ ] update git stuff when active project changes
- [ ] display active project and active vi
- [ ] show progress not with bar but in text field at bottom.
- [ ] add list and allow drag/drop to get blueignore
- [ ] same window add ability to select multiple and create blueignore file.
- [ ] add text file that defines order of steps to load.
- [ ] wire bends per felipe's wire bends on his lv-dev-hooks 


- [ ] align inputs on L by R edge and outputs on L by R edge
- [ ] align vertically any input-output that are directly connected
- [ ] for class property nodes - remove error case structures.
- [ ] blank icon to set vi icon to name
- [ ] all (visible) labels size to text
 
future
- [ ] Consider adding interactive? Steps - that would allow for things like - missing description, then popup a texteditor OR default icon, popup icon editor - those are the 2 I am immediately thinking of.