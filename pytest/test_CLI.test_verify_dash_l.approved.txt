--VI STEPS--
Separate Compiled Code From Source File
Auto Error Handling
Autogrow
Front Panel Color
Icon View
Move Error Wires Back
Remove No Error
Subdiagram Label Justify
Terminal Labels on Side
Terminal Labels Transparent
Replace Empty String Constant
Arrange FP
Arrange BD

--LIBRARY STEPS--
Separate Compiled Code For Library

--PROJECT STEPS--
Separate Compiled Code For Project

--MACROS--
ALL
GUI
NATTIFY
