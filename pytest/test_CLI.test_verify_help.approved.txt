[34;49;24m+--------------------------+
|    ____  _               |
|   |  _ \| |              |
|   | |_) | |_   _  ___    |
|   |  _ <| | | | |/ _ \   |
|   | |_) | | |_| |  __/   |
|   |____/|_|\__,_|\___|   |
|                          |
|   by SAS Workshops       |
+--------------------------+
[0m
Blue - Like Black but for LabVIEW
Version: x.x.x.x
Path: blue.vi

Blue is an autoformatter for LabVIEW.

ADMINISTRATIVE OPTIONS
NOTE: These don't process files, just do their thing and exit
--help, -h	Help Information
--version	Version Information
--list, -l	List All Available Steps and Macros
--path, -p	Path where blue.vi was found - mostly for debugging
--description	Get the description for a Step or Macro

FILE PROCESSING OPTIONS
--only, -o	Only run the specified tests. All ignores still apply. Will accept a CSV list. Place the entire list in double quotes for best results.
--ignore, -i	Ignore the specified tests. Will accept a CSV list. Place the entire list in double quotes for best results.
--strict, -s	Unrecognized Step and Macro names generate errors instead of warnings. Can be turned on via CLI flag or via blue_conf.ini in users home directory. Set strict=True under the [blue] section in the ini file to turn it on.
--force, -f	Often used with --only Runs tests regardless of ignores. Use with caution.

VERBOSITY
-q	Quiet
-v	Verbose
-vv	Very Verbose
-vvv or -d	Debug

USAGE
g-cli blue -- [OPTIONS] ["path to directory or file to format"]
