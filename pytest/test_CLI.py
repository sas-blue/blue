import subprocess
from os import environ
from approvaltests import verify, Options
from approvaltests.scrubbers import create_regex_scrubber
import pytest
import shlex
import pathlib


@pytest.fixture()
def this_dir():
    return str(pathlib.Path(__file__).parent.resolve())


@pytest.fixture()
def scrubbers(this_dir):
    path_scrubber = create_regex_scrubber(this_dir.replace("\\", "\\\\") + "\\\\", "")
    ms_scrubber = create_regex_scrubber(r"\d+\sms", r"xxxx ms")
    hms_scrubber = create_regex_scrubber(r"\d+:\d\d:\d\d", r"xx:xx:xx")
    saved_scrubber = create_regex_scrubber(r".*File Saved ->.*","")
    options = (
        Options()
        .add_scrubber(path_scrubber)
        .add_scrubber(ms_scrubber)
        .add_scrubber(hms_scrubber)
        # .add_scrubber(saved_scrubber)
    )
    return options


@pytest.fixture()
def blue_cmd():
    return [
        "g-cli",
        "blue",
        "--",
        "-d",
    ]


@pytest.fixture()
def run_blue(blue_cmd):
    def run(options_str):
        return subprocess.run(
            blue_cmd + shlex.split(options_str), capture_output=True, text=True
        )

    return run


@pytest.mark.ci_only
def test_version(run_blue):
    blue_output = run_blue("--version")
    assert blue_output.stdout.strip() == environ["FULL_VERSION"]
    assert blue_output.returncode == 0


@pytest.mark.ci_only
def test_hash(run_blue):
    git_output = subprocess.run(
        ["git", "rev-parse", "HEAD"], capture_output=True, text=True
    )
    blue_output = run_blue("--hash")
    assert blue_output.stdout.strip() == git_output.stdout.strip()
    assert blue_output.returncode == 0


def test_verify_with_no_params(run_blue):
    """
    Given - no CLI parameters
    When - I call blue
    Then -  It spits out the help message and no error
    """
    blue_output = run_blue("")
    version_scrubber = create_regex_scrubber(
        r"Version:\s\d+\.\d+\.\d+\.\d+", "Version: x.x.x.x"
    )
    path_scrubber = create_regex_scrubber(r"Path:.+blue.vi", "Path: blue.vi")
    verify(
        blue_output.stdout.strip(),
        options=Options().add_scrubber(version_scrubber).add_scrubber(path_scrubber),
    )
    assert blue_output.returncode == 0

''' TODO Everything below here can probably be moved to LUnit and maybe not all tests have to be E to E, maybe we can trim the list down.
Problem is doing everything EtoE causes me to have to edit the approved files everytime I add a new Step. At least 8 tests fail and have to get updated. Would be nice to trim that list down to maybe a handful.
And also have that trip up the LUnit instead of these tests.
Now when I add a step, when these fail I have rerun these test but they run after the build and are a pain.
I'd like to be able to just run the LUnit tests and then push and not have to run these locally and have high confidence that everything is good and have these running in CI just as a backup.
'''
def test_verify_blueignore_comment_ALL(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue on a file with an #blueignore ALL comment
    Then - then  file is still processed but all steps are ignored.
    """
    blue_output = run_blue(f'"{this_dir}/Sample VIs/Ignore Comments/blueignore ALL.vi"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_only_NATTIFY(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue on a file with an #blueignore ALL comment
    Then - then  file is still processed but all steps are ignored.
    """
    blue_output = run_blue(f'--only NATTIFY "{this_dir}/Sample VIs/Empty VI.vi"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_works_on_vit(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue on a file on a vit
    Then - then it works like a normal file.
    """
    blue_output = run_blue(f'"{this_dir}/Sample VIs/Empty VI.vit"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_works_on_vim(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue on a vim
    Then - it is processed like a normal file
    """
    blue_output = run_blue(f'"{this_dir}/Sample VIs/Example VIM.vim"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_blueignore_multiple(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue on a file with several #blueignore comments including a list and a macro
    Then - The ignores are all recognized and combined into one list and the other steps are still run.
    """
    blue_output = run_blue(
        f'"{this_dir}/Sample VIs/Ignore Comments/blueignore GUI Autogrow Arrange BD.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_missing_BDs_ignored(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue on several files without BDs one missing, one PW protected
    Then - I should get a warning message and no tests run, but no error
    """
    blue_output = run_blue(f'"{this_dir}/Sample VIs/Missing BD"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_blueignore_comment_GUI_gets_expanded(run_blue, this_dir):
    """
    Given -
    When - I call blue on a file with an #blueignore GUI comment
    Then - Then the GUI macro gets expanded and those steps get ignored.
    """
    blue_output = run_blue(f'"{this_dir}/Sample VIs/Ignore Comments/blueignore GUI.vi"')
    assert "BookMarks Found" in blue_output.stdout
    assert "Ignored Steps -> Arrange FP, Front Panel Color" in blue_output.stdout
    assert "Step Starting -> Front Panel Color" not in blue_output.stdout
    assert "Step Starting -> Arrange FP" not in blue_output.stdout
    assert blue_output.returncode == 0


def test_verify_only_autogrow(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue with --only autogrow
    Then - Then autogrow is the only step run
    """
    blue_output = run_blue(f'--only Autogrow "{this_dir}/Sample VIs/Empty VI.vi"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_only_autogrow_multiple(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue --only with a list including a macro
    Then - then  file is still processed but specified steps are ignored.
    """
    blue_output = run_blue(
        f'--only "Autogrow, GUI" "{this_dir}/Sample VIs/Empty VI.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_only_matches_ignore(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue --only with a macro
    Then - then  file is still processed but macro is expanded and specified steps are ignored.
    """
    blue_output = run_blue(
        f'--only GUI "{this_dir}/Sample VIs/Ignore Comments/blueignore GUI.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_only_matches_ignore_with_force(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue --only with a macro and --force and it matches an ignore comment
    Then - then  file is still processed but macro is expanded and the only tests are run in spite of the ignore comment
    """
    blue_output = run_blue(
        f'--only GUI --force "{this_dir}/Sample VIs/Ignore Comments/blueignore GUI.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_force_with_no_only(run_blue, this_dir, scrubbers):
    """
    Given -
    When - I call blue --force on a VI that has an ignore comment
    Then - then  file is still processed but macro is expanded and specified steps are not ignored because of the force.
    """
    blue_output = run_blue(
        f'-- --force "{this_dir}/Sample VIs/Ignore Comments/blueignore GUI.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_blueignore_files(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a bunch of .blueignore files and random VIs
    When - I call blue
    Then - .blueignores stack for each directory and appropriate tests are ignored.
    """
    blue_output = run_blue(f'"{this_dir}/Sample VIs/blueignore files"')
    verify(blue_output.stdout.strip(), options=scrubbers)
    assert blue_output.returncode == 0


def test_verify_only_and_blueignore(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a bunch of .blueignore files and random VIs
    When - I call blue with only and one of the tests specified by the blueignore
    Then - blue ignores the only test anyway ie blue ignore takes precedent
    """
    blue_output = run_blue(
        f'--only "Arrange FP" "{this_dir}/Sample VIs/blueignore files/GUI.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)


def test_verify_ignore_and_blueignore(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a bunch of .blueignore files and random VIs
    When - I call blue with ignore
    Then - blue ignores the ignore steps and the .blueignore steps
    """
    blue_output = run_blue(
        f'--ignore "Arrange BD" "{this_dir}/Sample VIs/blueignore files/GUI.vi"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)

def test_verify_only_on_library(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a single library
    When - I call blue with only and "Separate Compiled Code From Library" 
    Then - blue ignores all the VIs and only processes the library file.
    """
    blue_output = run_blue(
        f'--only "Separate Compiled Code For Library" "{this_dir}/Sample VIs/"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)

def test_force_on_library(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a single library
    When - I call blue with only and "Separate Compiled Code From Library" 
    Then - blue ignores all the VIs and only processes the library file.
    """
    blue_output = run_blue(
        f'--only "Separate Compiled Code For Library" --force "{this_dir}/Sample VIs/Ignore Separate Compiled For Library"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)


def test_verify_only_on_project(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a single project
    When - I call blue with only and "Separate Compiled Code From Project" 
    Then - blue ignores all the VIs and only processes the library file.
    """
    blue_output = run_blue(
        f'--only "Separate Compiled Code For Project" "{this_dir}/Sample VIs/"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)

def test_force_on_project(run_blue, this_dir, scrubbers):
    """
    Given - a directory structure with a single library
    When - I call blue with only and "Separate Compiled Code From Library" 
    Then - blue ignores all the VIs and only processes the library file.
    """
    blue_output = run_blue(
        f'--only "Separate Compiled Code For Project" --force "{this_dir}/Sample VIs/Ignore Separate Compiled For Project"'
    )
    verify(blue_output.stdout.strip(), options=scrubbers)